const connection = require('./server/mongo_connect');
const ObjectID   = require('mongodb').ObjectID;

let user_data = [
    {
        login: 'assistant1@uzchinatrade.com',
        mark: 90
    },
    {
        login:'bakhodir@uzchinatrade.com',
        mark: 90
    },
    {
        login: 'finance@uzchinatrade.com',
        mark: 90
    },
    {
        login :'fin.acc@clg.uz',
        mark: 100
    },
    {
        login: 'booker@uzchinatrade.com',
        mark: 100
    },
    {
        login: 'booker3@uzchinatrade.com',
        mark: 100
    },
    {
        login: 'zuhra@uzchinatrade.com',
        mark: 100
    },
    {
        login: 'finance_monitoring@uzchinatrade.com',
        mark: 95
    },
    {
        login: 'kassa2@uzchinatrade.com',
        mark: 100
    },
    {
        login: 'gavkhar@uzchinatrade.com',
        mark: 90
    },
    {
        login: 'veronika@uzchinatrade.com',
        mark: 94.4,
    },
    {
        login: 'hr2@humopartners.uz',
        mark: 82
    },
    {
        login: 'aleksey@uzchinatrade.com',
        mark: 100
    },
    {
        login: 'sales8@uzchinatrade.com',
        mark: 25.3
    },
    {
        login: 'contract4@uzchinatrade.com',
        mark: 87.9
    },
    {
        login: 'sales3@uzchinatrade.com',
        mark: 87
    },
    {
        login: 'logistics@uzchinatrade.com',
        mark: 73
    },
    {
        login: 'ahror@uzchinatrade.com',
        mark: 77
    },
    {
        login: 'sales@uzchinatrade.com', 
        mark: 82
    },
    {
        login: 'sales6@uzchinatrade.com',
        mark:59
    },
    {
        login: 'sales1@uzchinatrade.com',
        mark:70
    },
    {
        login: 'nokil@uzchinatrade.com',
        mark:80
    },
    {
        login:  'contract3@uzchinatrade.com',
        mark: 92
    },
    {
        login: 'service1@clg.uz',
        mark:86
    },
    {
        login: 'guarantee@uzchinatrade.com',
        mark: 85
    },
    {
        login: 'dilshod@clg.uz',
        mark :100
    },
    {
        login: 'shahdiyor@uzchinatrade.com',
        mark: 100
    },
    {
        login: 'monitoring@clg.uz',
        mark: 100
    },
    {
        login: 'sales1@clg.uz',
        mark:90.2
    },
    {
        login: 'monitoring1@clg.uz',
        mark: 100
    },
    {
        login: 'manager@clg.uz',
        mark: 100
    },
    {
        login: 'nazira@ibt.uz',
        mark: 87
    },
    {
        login: 'sales03@ibt.uz',
        mark: 81
    },
    {
        login: 'sales@ibt.uz',
        mark: 93,
    },
    {
        login: 'pr@ibt.uz',
        mark: 97,
    },
    {
        login: 'touroperator@ibt.uz',
        mark :100
    },
    {
        login: 'vitaliy@groundzero.uz',
        mark: 72.5,
    },
    {
        login: 'yaroslava@groundzero.uz',
        mark: 69.5
    },
    {
        login:'madina@groundzero.uz', 
        mark: 53
    },
    {
        login:'dilbar@groundzero.uz',
        mark: 85,
    },
    {
        login:'nigora@groundzero.uz',
        mark :75
    },
    {

        login: 't.izgutdinov@groundzero.uz',
        mark:51.67
    },
    {

        login: 'm.hayitova@groundzero.uz',
        mark: 41.7
    },
    {

        login: 'saodat@groundzero.uz',
        mark: 40
    },
    {

        login: 'igor@uzchinatrade.com',
        mark: 98
    },
    {
        login: 'igor.k@uzchinatrade.com',
        mark: 89
    }
]



let fun = async () => {
    let db = await new connection
    let dbase      = await db.db('humo-kpi');
    let users      = dbase.collection('users');
    let kpis       = dbase.collection('kpis');

    user_data.forEach(async u => {
        try {
            let user = await users.findOne({login: u.login});
            if(user) {
                let updateKpi = await kpis.updateOne({user_id: ObjectID(user._id), year: "2019", period: "2"}, {
                    $set: {
                        total: u.mark
                    }
                }, {upsert: true})
                console.log(updateKpi)
            }
        } catch (err){
            throw err;
        }
    })
}

fun();
