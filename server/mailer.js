const nodemailer = require("nodemailer");

var  main = async (data) => {
  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport({
    host: 'smtp.yandex.ru',
    port: 465,
    secure:true,
    auth: {
      user: 'kpi@hmpartners.uz',
      pass: 'info2699339'
    },
    sendMail: true
  });

  // "Hey, "+data.surname+' '+data.name+"\nМы будем рады отправлять вам уведомления о ваших." +
  //   "Также есть и другие способы информировать вас об отчетах - Например телеграм бот"
  // "Приветствуем вас в системе оценок HUMO Partners ✔
  
  let info = await transporter.sendMail(data);
  // {
  //   from: 'HUMO PARTNERS <kpi@humopartners.uz>', // sender address
  //   to: data.address, // list of receivers
  //   subject: 'HUMO Partners КПИ', // Subject line
  //   text: data.text, // plain text body
  //   html: data.html // html body
  // }
  // console.log("Message sent: %s", info.messageId);
  return info.messageId;
  // return 0;
  // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
}

// main().catch(console.error);
module.exports = main;