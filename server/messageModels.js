const sendMail   = require('./mailer');

var messages = (employee, msg) => {
    var resultText = '';
    switch(msg){
        case 1: 
            resultText = `
            <h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
                ${employee} отправил(а) Вам на рассмотрение форму KPI.  Пожалуйста, войдите в личный кабинет: http://kpi.hmpartners.uz
                для того чтобы рассмотреть, согласовать или отправить на доработку.  
                <br>
                <br>Сообщение сгенерировано автоматически.<br>  
                <br>
                 KPI</p>`
        ;
        break;
        case 2:
            resultText = `
            <h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
            <br>
            ${employee} отправил(а) Вам на доработку форму KPI.  Пожалуйста, войдите в личный кабинет:  http://kpi.hmpartners.uz для того чтобы внести изменения и отправить заполненную форму на утверждение.  
            <br>
            <br>Сообщение сгенерировано автоматически.<br>  
            <br>
            KPI</p>`
        ;
        break;
        case 3:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
            <br>
             
            ${employee} отправил(а) Вам на рассмотрение форму KPI с исправлениями.  Пожалуйста, войдите в личный кабинет:  http://kpi.hmpartners.uz для того чтобы рассмотреть, согласовать или отправить на доработку.  
            <br>
             
            <br>Сообщение сгенерировано автоматически.<br>  
             
            <br>
            KPI</p>`
        ;
        break;
        case 4:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
             
            <br>
            Ваши KPI утверждены .  
            <br>
             
            <br>Сообщение сгенерировано автоматически.<br>  
            <br>
            KPI</p> `
        ;
        break;
        case 5:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
            <br>
             
            ${employee} отправил(а) Вам на рассмотрение отчет по KPI.  Пожалуйста, войдите в личный кабинет:  http://kpi.hmpartners.uz для того чтобы рассмотреть, согласовать или отправить на доработку.  
             
            <br>
            <br>Сообщение сгенерировано автоматически.<br>  
             
            <br>
            KPI</p> `
        ;
        break;
        case 6:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
            <br>
             
           ${employee} отправил(а) Вам на доработку отчет по KPI.  Пожалуйста, войдите в личный кабинет:  http://kpi.hmpartners.uz для того чтобы внести изменения и пере отправить отчет на утверждение.  
           <br>
             
            <br>Сообщение сгенерировано автоматически.<br>  
             
            <br>
            KPI</p> `
        ;
        break;
        case 7:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
            <br>
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
             
            <br>
            ${employee} отправил(а) Вам на рассмотрение отчет по KPI с исправлениями.  Пожалуйста, войдите в личный кабинет:  http://kpi.hmpartners.uz для того чтобы рассмотреть, утвердить  или отправить на доработку.  
             
            <br>
            <br>Сообщение сгенерировано автоматически.<br>  
             
            <br>
            KPI</p> `
        ;
        break;
        case 8:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
             
            <br>
            Ваш отчет по KPI принят.  
             
            <br>
            <br>Сообщение сгенерировано автоматически.<br> 
            <br>
            KPI</p> `
        ;
        break;
        case 9:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
            <br>
             
            Ваши итоги по KPI подведены. Результаты доступны в личном кабинете в разделе ИТОГИ 
            <br>
             
            <br>
            <br>Сообщение сгенерировано автоматически.<br>
            <br>
            KPI</p> `
        ;
        break;
        case 10:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
             
            <br>
            Вы заполнили формы оценки компетенций ${employee}. Вам необходимо встретиться для подведения итогового результата.  
             
            <br>
            Предварительные результаты по 2м оценкам доступны в личном кабинете в разделе ОЦЕНКИ СОТРУДНИКОВ  или  КОМПЕТЕНЦИИ 
             
            <br>
            <br>Сообщение сгенерировано автоматически.<br>
            <br>
            KPI</p>`
        ;
        break;
        case 11:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
             
            <br>
            Оценка ваших компетенций завершена.  Результаты доступны в личном кабинете в разделе ИТОГИ 
             
            <br>
            <br>Сообщение сгенерировано автоматически.<br>
            <br>
            KPI</p> `
        ;
        break;
        case 12:
            resultText = `<h3>Информационное сообщение сайта kpi.hmpartners.uz</h3> 
 
 
            <p style="font-size:16px;color:black">    
            Здравствуйте!<br> 
             
            <br> 
            Дониёр Давлатович,   Вам необходимо  провести оценку компетенций руководителей направлений, а затем встретиться с руководителями направлений для подведения итоговых результатов по компетенциям.  
            <br>
             
            Оценка сотрудниками непосредственных руководителей завершена. Итоги доступны во вкладке ОЦЕНКИ СОТРУДНИКОВ 
            <br>
            <br>Сообщение сгенерировано автоматически.<br>
            <br>
            KPI</p> 
            `
        ;
        break;
        default:
            resultText = "OK";
    }
    return resultText;
};

var types = [
    'КПИ',
    'Компетенции',
]
let mail = (data) => ({
    from: 'HM PARTNERS <kpi@hmpartners.uz>',  // sender address
    to: data.to,                                  // list of receivers
    subject: `HM Partners ${types[data.type]}`, // Subject line
    // text: `<p>`+messages(data.employee, data.msg)+`</p>`,      // plain text body
    html:messages(data.employee, data.msg)+` <br><br><a href="http://kpi.hmpartners.uz/" style="color:red;">Перейти по ссылке</a>` // html body
});

// let send = async () => {

//     let email = {
//         type: 1,
//         employee: ` Farkhad`,
//         msg: 1,
//         to: 'maxaquentin@gmail.com'
//     };
    
//     let sentMail = await sendMail(mail(email));
//     console.log(mail(email))
//     console.log(sentMail)
// }
// send();
module.exports = mail;
