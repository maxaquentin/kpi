var kpi_form  = {
last_edit_time : '',
last_edit_person:'' ,
status: 'Новая',
fact_approvements: [],
goals: [
    {
        name: '', 
        kpi:'',
        weight: 0,
        units: '',
        plan_value: '',
        fact_value: '',
        goal_achieved: 0,
        goal_mark: '',
        months: [
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
            {
                plan: 0,
                fact: 0,
                achieved:0
            },
        ] 
    },
],
comments: [], 
report_comments: [] 
}
module.exports = kpi_form;