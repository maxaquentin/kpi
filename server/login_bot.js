const Telegraf         = require('telegraf')
const tg_token         = '870772674:AAHkxiSH3auTtmxgJkU5QYgFFzNkOvD1lc4';
const DB               = require('./mongo_connect');
const bot              = new Telegraf(tg_token)
const { zip } = require('zip-a-folder');
const TelegrafFlow     = require('telegraf-flow')
const { Scene }        = TelegrafFlow
const flow             = new TelegrafFlow()
const session          = require('telegraf/session')

var db    = null;
var dbase = null;
var bot_users = null;
var users = null;

bot.use(async (ctx, next) => {
    db        = await new DB();
    dbase     = await db.db('humo-kpi');
    users     = await dbase.collection('users');
    bot_users = await dbase.collection('bot-users');
    next();
})

bot.use(session());
bot.use(flow.middleware());

// Scenes initiation
const login            = new Scene('login');
const password         = new Scene('password');
const backups            = new Scene('backups');


bot.command('login', (ctx) => {ctx.flow.enter('login')})
bot.command('backups', (ctx) => {ctx.flow.enter('backups')})
login.command('login', (ctx) => {ctx.flow.enter('login')})
password.command('login', (ctx) => {ctx.flow.enter('login')})

bot.command('get_backups', async (ctx) => {
    let user = await bot_users.findOne({chat_id: ctx.from.id});
    if(user && user.logged_in && user.backups){
        await zip('../db_backups/humo-kpi', '../db_backups/humo-kpi.zip')
        ctx.replyWithDocument({ source: '../db_backups/humo-kpi.zip' });
    } else {
        await ctx.reply('Требуется авторизация');
        ctx.flow.enter('login');
    }
})

login.enter(ctx => {
    ctx.reply('Введите адрес эл. почты:');
})

login.on('message', async ctx => {
    try {
        let user = await users.findOne({login: ctx.message.text});
        if(user) {
            ctx.session.state = { user : user };
            ctx.flow.enter('password');
        }
        else ctx.reply('Извините, но такой адрес не найден. Введите другой адрес:')
    } catch (err){
        throw err;
    }
})

password.enter(ctx => {
    ctx.reply('Введите пароль: ');
})


password.on('message', async ctx => {
    try {
        if(!ctx.session.state) {
            ctx.flow.enter('login');
            return;
        }
        let user = ctx.session.state.user;
        let msg  = ctx.message.text;
        if(user.password === msg) {
            await bot_users.updateOne({chat_id: ctx.from.id}, {
                $set: {
                    login: user.login,
                    logged_in: true,
                    backups: false,
                    messages: false,
                    chat_id: ctx.from.id,
                }
            }, {upsert: true});
            ctx.reply('Авторизация прошла успешно!')
            ctx.flow.leave('password');
        }
        else ctx.reply('Пароль неверный');
        } catch (err){
        throw err;
    }
})

backups.enter(ctx => {
    ctx.reply('Введите пароль доступа базы данных: ');
})

backups.on('message', async ctx => {
    try {
        let msg  = ctx.message.text;
        let user = await bot_users.findOne({chat_id: ctx.from.id});
        if(user && user.logged_in){
            if(msg === '123hpkpi-asjbYUsdgy73') {
                await bot_users.updateOne({chat_id: ctx.from.id}, {
                    $set: {
                        backups: true,
                    }
                }, {upsert: true});
                ctx.flow.leave('backups');
                await zip('../db_backups/humo-kpi', '../db_backups/humo-kpi.zip')
                ctx.reply('Отлично! Бот будет отправлять вам файлы базы данных каждую неделю.')   
                ctx.replyWithDocument({ source: '../db_backups/humo-kpi.zip' });
            }
            else ctx.reply('Пароль неверный');
        } else {
            await ctx.reply('Требуется авторизация');
            ctx.flow.enter('login');
        }
    } catch (err){
        throw err;
    }
})

bot.command('messages', async ctx => {
    try {
        let msg  = ctx.message.text;
        let user = await bot_users.findOne({chat_id: ctx.from.id});
        if(user && user.logged_in){
            await bot_users.updateOne({chat_id: ctx.from.id}, {
                $set: {
                    messages: true,
                }
            }, {upsert: true});
            ctx.reply('Теперь к вам будут приходить уведомления от kpi.humopartners.uz');
        } else {
            await ctx.reply('Требуется авторизация');
            ctx.flow.enter('login');
        }
    } catch (err){
        throw err;
    }
})


// SCENE register
flow.register(login);
flow.register(password);
flow.register(backups);

bot.start(async ctx => {
    let inserted = await bot_users.updateOne({chat_id: ctx.from.id}, {
        $set: {
            logged_in: false,
            backups: false,
            messages: false,
            login: null,
            chat_id: ctx.from.id,
        }
    }, {upsert: true});
    ctx.flow.enter('login')
})

bot.startPolling();
