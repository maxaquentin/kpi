const Telegram   = require('telegraf/telegram')
const tg_token   = '870772674:AAHkxiSH3auTtmxgJkU5QYgFFzNkOvD1lc4';
const { zip }    = require('zip-a-folder');
let bot_options  = {
    agent: null,
    webhookReply: true
  }

let   bot        = new Telegram(tg_token, bot_options);
const DB         = require('./mongo_connect');

let send = async () =>  {
    try {
        let db        = await new DB();
        let dbase     = await db.db('humo-kpi');
        let bot_users = await dbase.collection('bot-users')
        
        let helpers      = await bot_users.find({backups: true}).toArray();
        await zip('/home/kpi/db_backups/humo-kpi', '/home/kpi/db_backups/humo-kpi.zip')
        for(var i=0;i<helpers.length;i++){
            await bot.sendMessage(helpers[i].chat_id, `База данных за ${new Date().toString()} `);
            bot.sendDocument(helpers[i].chat_id, { source: '/home/kpi/db_backups/humo-kpi.zip' });
        }
    } catch (err) {
        throw err;
    }
}

send();