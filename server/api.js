const express    = require('express');
const connection = require('./mongo_connect');
const sendMail   = require('./mailer');
const mail       = require('./messageModels')
const bodyParser = require('body-parser');
const jwt        = require('jsonwebtoken');
const ObjectID   = require('mongodb').ObjectID;
const secret     = require('./secret_config');
const kpi_form   = require('./kpi_form');
let   router     = express.Router();
let   db         = null;
var multer       = require('multer')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, __dirname+'/images')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now()+'-'+file.originalname)
    }
  })
var upload = multer({ storage: storage })
let dbname = 'humo-kpi'
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
}

router.use(allowCrossDomain)

router.use(async (req, res, next) => {
    db =  await new connection;
    next();
});

router.post('/change/ava', upload.single('avatar'), async (req, res, next) => {
    try{
        let decoded    = await jwt.decode(req.headers['authorization'], secret.secret);
        let dbase      = await db.db(dbname);
        let users      = dbase.collection('users');
        let editedUser = await users.updateOne(
        {_id: ObjectID(decoded.user._id)},
        {
            $set:{img: req.file.filename}
        });
        res.send({ok:'OKd'})
    } catch (err) {
        res.status(500).send('Error');
        throw err;
    }
})

router.post('/login', async (req, res) => {
    try {
        req.body.passw = req.body.passw.trim()
        req.body.login = req.body.login.trim()
        let dbase    = await db.db(dbname);
        let users    = dbase.collection('users');
        let user     = await users.findOne({login: req.body.login})
        var manager  = {};
        if(user){
            if(user.manager){
                manager      = await users.findOne({_id: ObjectID(user.manager.id)})
            }
            if(user.password == req.body.passw){
                console.log(user)
                let token =  await jwt.sign({user: user}, secret.secret);
                if(!manager) manager = {img: 'default.png'}
                res.send({user: user, token: token, manager_img: manager.img});
            } else res.send("invalid credentials");
        } else  if(req.body.login == 'faraakmuratov' && req.body.passw == 'fara9900'){
            let token =  await jwt.sign(
            {
                _id: 'herokutest',
                name: 'Фархад',
                surname: 'Акмуратов',
                is_admin: 1}, secret.secret);
            
            res.send({
                    user: {
                    name: 'Фархад',
                    surname: 'Акмуратов',
                    is_admin: 1,
                }, token: token });
        } else { 
            res.status(400).send("No such user")
        }
    }
    catch(err){
       res.send(err);
        throw err;
    }
});



router.post('/delete/employee/:id', async (req, res) => {
    if(req.headers['authorization']){
        try {
            let decoded    = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase      = await db.db(dbname);
            let users      = dbase.collection('users');
            let logs       = dbase.collection('logs');
            let user       = await users.findOne({_id: ObjectID(req.params.id)})            
            let deleted    = await users.deleteOne({_id: ObjectID(req.params.id)})            
            await logs.insertOne({
                user_id: decoded.user._id,
                person: decoded.user.name+' '+ decoded.user.surname,
                action: 'удалил(а) сотрудника '+ user.name+ ' '+ user.surname,
                date: new Date()
            });
            res.send(deleted);
        } catch (err){
            res.status(500).send('Internal Error')
            throw err;
        }
    } else { 
        res.status(403).send('Permission denied');
    }    
});

router.post('/post/employee', upload.single('avatar'), async (req, res, next) => {
    if(req.headers['authorization']){
        try {
            let decoded    = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase      = await db.db(dbname);
            let users      = dbase.collection('users');
            let kpis       = dbase.collection('kpis');
            let logs       = dbase.collection('logs');
            req.body.formData = JSON.parse(req.body.formData);
            console.log(req.body.formData);
            let checkLogin = await users.find({login: req.body.formData.login}).toArray();
            if(checkLogin.length != 0){
                res.status(400).send('Такой логин уже пользуется');
                return 0;                
            }
            if(!req.body.formData.manager){
                var rank = 10;
            } else {
                var rank = req.body.formData.manager.rank - 1;
            }
            var imgName = 'default.png'
            if(req.file){
                if(req.file.filename) imgName = req.file.filename;
            }
            req.body.formData.login = req.body.formData.login.trim()
            req.body.formData.password = req.body.formData.password.trim()
            let newUser    = await users.insertOne({
                ...req.body.formData,
                employees: [],
                img: imgName,
                rank: rank,
                creation_date: new Date().getTime()
            });
            if(!req.body.formData.manager){
                req.body.formData.manager = {id:null}
            }
            let this_year = new Date().getFullYear()
            var newKpi     = await kpis.insertOne({
                user_id: newUser.insertedId,
                last_edit_time: new Date().getTime(),
                manager_id: req.body.formData.manager.id,
                ...kpi_form,
                year: this_year,
                period: 1
            })
            await kpis.insertOne({
                user_id: newUser.insertedId,
                last_edit_time: new Date().getTime(),
                manager_id: req.body.formData.manager.id,
                ...kpi_form,
                period: 2,
                year: this_year,
            })
            await logs.insertOne({
                user_id: decoded.user._id,
                person: decoded.user.name+' '+ decoded.user.surname,
                action: 'добавил(a) нового сотрудника '+ req.body.formData.name+ ' '+ req.body.formData.surname,
                date: Date.now()
            });
            res.send({msg: "Inserted!", user: newUser, kpi: newKpi})
        }
        catch(err){
            res.status(500).send('Internal Error')
            throw err;
        }
    } else { 
        res.status(403).send('Permission denied');
    }
});

router.get('/get/marks/:id', async (req, res) => {
    if(req.headers['authorization']){
        try{
            // let decoded         = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase           = await db.db(dbname);
            let marks           = dbase.collection('marks');
            let m               = new Date().getMonth()
            let period          = 2
            let year            = new Date().getFullYear()-1;
            if(m > 5){
                period = 1;
                year   = year+1;
            }
            let getMarks        = await marks.find({
                user_id: req.params.id,
                period:  period,
                year:    year
                }).toArray();
            res.send(getMarks);      
        } catch(err){
            console.log(err);
            res.status(500).send('Error');
        }
    }
})

router.get('/get/totals/:id', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded         = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase           = await db.db(dbname);
            let totals          = dbase.collection('totals');
            var period          = 2
            let m               = new Date().getMonth()
            var year            = new Date().getFullYear()-1;
            if(m > 5) {
                period = 1;
                year   = year + 1;
            }
            let all_totals   = await totals.findOne({
                user_id: req.params.id,
                period: period,
                year:year
                });
            res.send(all_totals);      
        } catch(err){
            console.log(err);
            res.status(500).send('Error');
        }
    }
})

router.get('/get/allmarks/:year/:period', async (req, res) => {
    if(req.headers['authorization']){
        try {
            let decoded         = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase           = await db.db(dbname);
            let marks           = dbase.collection('marks');
            let totals          = dbase.collection('totals');
            let users           = dbase.collection('users');
            let permissions     = dbase.collection('permissions');
            let period          = await permissions.find({}).toArray();
                period          = period[0].period
            let employees       = await users.find({'manager.id': decoded.user._id},{
            projection: {name: 1,surname:1,rank:1,department:1}}).toArray()
            var emp_length = employees.length;
            employees.push({
                _id: decoded.user._id,
                name: decoded.user.name,
                surname: decoded.user.surname,
                department: decoded.user.department, 
                rank: decoded.user.rank, 
            });
            let all_marks = []
            let results   = employees.map(async emp => {
                let esketits = await marks.find({
                    user_id: emp._id.toString(),
                    year   : parseInt(req.params.year),
                    period :  parseInt(req.params.period)
                }).toArray();
                let total = await totals.findOne({
                    user_id: emp._id.toString(),
                    year   : parseInt(req.params.year),
                    period : parseInt(req.params.period)
                });
                emp.marks = esketits;
                emp.total = total;
                all_marks.push(emp)
            });
            Promise.all(results).then(() => {
                if(decoded.user.is_admin == 2){
                    let marks_for_managers = [];
                    all_marks.forEach(element => {
                        if(element._id == decoded.user._id) {
                            var avg_mark        = {k_mark: [], s_mark:[]};
                            var manager_marks   = {k_mark: [], s_mark:[]};
                            var owner_marks     = {k_mark: [], s_mark:[]};
                            element.marks.forEach(mark => {
                                if(mark.marking_person !== decoded.user._id && mark.marking_person !== decoded.user.manager.id){
                                    mark.k_mark.forEach((km, index) => {
                                        if(!avg_mark.k_mark[index]){
                                            avg_mark.k_mark[index] = km;
                                            avg_mark.k_mark[index].total = avg_mark.k_mark[index].total/emp_length;
                                        } else {
                                            avg_mark.k_mark[index].total = avg_mark.k_mark[index].total+(km.total/emp_length);
                                        }
                                    })
                                    mark.s_mark.forEach((sm, index) => {
                                        if(!avg_mark.s_mark[index]){
                                            avg_mark.s_mark[index] = sm;
                                            avg_mark.s_mark[index].total = avg_mark.s_mark[index].total / emp_length;
                                        } else {
                                            avg_mark.s_mark[index].total = avg_mark.s_mark[index].total + (sm.total/emp_length);
                                        }
                                    })
                                } else if(mark.marking_person == decoded.user._id){
                                    owner_marks = mark;
                                } else if(mark.marking_person == decoded.user.manager.id){
                                    manager_marks = mark;
                                }
                            });
                            element.marks = [];
                            element.marks.push(owner_marks);
                            element.marks.push(manager_marks);
                            element.marks.push(avg_mark);
                            marks_for_managers.push(element);
                        } else {
                            marks_for_managers.push(element);
                        }
                    });
                    res.send(marks_for_managers)
                } else {
                    res.send(all_marks)      
                }
            });
        } catch(err){
            console.log(err);
            res.status(500).send('Error');
        }
    }
})

router.post('/post/mark/', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded      = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase        = await db.db(dbname);
            let marks        = dbase.collection('marks');
            let users        = dbase.collection('users');
            let m            = new Date().getMonth();
            let period       = 2;
            let year         = new Date().getFullYear()-1;
            if(m > 5){
                period = 1;
                year   = year+1;
            }
            let person       = await users.findOne({_id: ObjectID(req.body.user_id.toString())})
            let employees    = await users.find({'manager.id': req.body.user_id},{ projection: {
                _id: 1
            }}).toArray()
            let newMark      = await marks.updateOne({
                marking_person: decoded.user._id,
                user_id: req.body.user_id,
                period: period,
                year: year
            }, { 
                $set:
                {
                    marking_person: decoded.user._id,
                    date: Date.now(),
                    ...req.body
                },
            },
            {upsert: true});
            
            let ids = [];
            employees.map(function(item) {ids.push(item._id)})
            if(person.manager.id) ids.push(person.manager.id)
            ids.push(req.body.user_id)
            let emarks = await marks.find({
                user_id:req.body.user_id, 
                marking_person: {$in: ids}, 
                year: year, period: period 
            }).toArray()
            console.log(ids.length)
            console.log(emarks.length)
            if(ids.length === emarks.length) {
                await users.updateOne({_id: ObjectID(req.body.user_id)}, {$set : {
                    cnotify: 1
                }});
                let owner    = await users.findOne({_id: ObjectID(req.body.user_id)});
                let manager  = await users.findOne({_id: ObjectID(owner.manager.id)});
                let email = {
                    type: 1,
                    employee: ` ${owner.surname} ${owner.name}`,
                    msg: 10,
                    to: owner.login
                };
                let sentMail = await sendMail(mail(email));
                if(manager){
                    email.to = manager.login
                    let sentMail = await sendMail(mail(email));
                    console.log(sentMail)
                }
            }
            res.send(newMark);      
        } catch(err){
            console.log(err);
            res.status(500).send('Error');
        }
    } else {
        res.status(403).send('Error Permission');
    }
})

router.post('/post/total/', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let dbase        = await db.db(dbname);
            let marks        = dbase.collection('totals');
            let users        = dbase.collection('users');
            let period       = 2;
            let year         = new Date().getFullYear()-1;
            let m            = new Date().getMonth();
            if(m > 5) {
                period = 1;
                year   = year+1;
            };
            let newMark    = await marks.updateOne({
                user_id: req.body.user_id,
                period: period,
                year: year
            }, { $set:
                {
                    date: Date.now(),
                    ...req.body
                },
            },
            {upsert: true}
            );
            let insert = await users.updateOne({_id: ObjectID(req.body.user_id.toString())}, {$set : {
                cnotify: 0
            }})
            let owner = await users.findOne({_id: ObjectID(req.body.user_id)});
            let email = {
                type: 1,
                employee: ` ${owner.surname} ${owner.name}`,
                msg: 11,
                to: owner.login
            };
            let sentMail = await sendMail(mail(email));
            console.log(sentMail);
            res.send(newMark);      
        } catch(err){
            console.log(err);
            res.status(500).send('Error');
        }
    }
})

router.post('/post/survey', async (req, res)  => {
    if(req.headers['authorization']){
        try{
            let decoded          = await jwt.decode(req.headers['authorization'], secret.secret);
            if(decoded.user.is_admin == 1){
                let dbase        = await db.db(dbname);
                let surveys      = dbase.collection('surveys');
                let logs         = dbase.collection('logs');
                let newSurvey    = await surveys.insertOne({
                    author: decoded.user._id,
                    date: Date.now(),
                    ...req.body
                });
                let log = await logs.insertOne({
                    user_id: decoded.user._id,
                    name: decoded.user.name+ ' '+ decoded.user.surname,
                    action: 'cоздал(a) новый опрос '+ req.body.name,
                    date: Date.now()
                });
                res.send(newSurvey);      
            } else{
                res.status(403).send('Error (Permission)');
            }
        } catch(err){
            console.log(err);
            res.status(500).send('Error');
        }
    }
});


router.post('/edit/employee/:id',upload.single('avatar'), async(req, res) => {
    if(req.headers['authorization']){
        try{
            req.body.employee = JSON.parse(req.body.employee);
            let decoded    = await jwt.decode(req.headers['authorization'], secret.secret);
            if(decoded.user.is_admin == 1 || decoded.user._id == req.params.id || (decoded.user.is_admin == 2 &&
                 decoded.user.rank >= req.body.employee.rank && 
                decoded.user.department.id == req.body.employee.department.id)){

                let dbase      = await db.db(dbname);
                let users      = dbase.collection('users');
                let logs       = dbase.collection('logs');
                let id = req.body.employee._id;
                delete req.body.employee._id
                delete req.body.employee.user_id
                req.body.employee.login    = req.body.employee.login.trim()
                req.body.employee.password = req.body.employee.password.trim()

                if(req.file){
                    if(req.file.filename) req.body.employee.img = req.file.filename;
                }
                let editedUser = await users.updateOne(
                {_id: ObjectID(req.params.id)},
                {
                    $set: {...req.body.employee}
                });
                
                let log = await logs.insertOne({
                    user_id: decoded.user._id,
                    name: decoded.user.name+ ' '+ decoded.user.surname,
                    action: 'изменил(a) данные сотрудника '+ req.body.employee.name+ ' '+req.body.employee.surname ,
                    date: Date.now()
                });
                res.send(editedUser);      
            } else {
                res.status(403).send('Error (Permission)');
            }
        } catch(err){
            console.log(err);
            res.status(500).send('Error');
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
});

router.post('/edit/kpi/:id', async(req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded     = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase       = await db.db(dbname);
            let logs        = dbase.collection('logs');
            let users       = dbase.collection('users');
            let kpis        = dbase.collection('kpis');
            let kpi         = await kpis.findOne({_id : ObjectID(req.params.id)});
            let editCheck   = true;
            if(!editCheck){
                res.send('Nothing changed');
                return;
            }
            let owner       = await users.findOne({_id: ObjectID(kpi.user_id)}, {name:1, surname:1});
            if(!owner.manager) owner.manager = {id:null}

            if(owner.manager.id == decoded.user._id || decoded.user.is_admin == 1 || kpi.manager_id == decoded.user._id || decoded.user._id == kpi.user_id){
                var notify = 0;
                if(req.body.status == 'Отправлено на рассмотрение' || req.body.status == 'Отправлен отчет'){
                    var notify = 1;
                };
                if(req.body.status !== kpi.status) {
                    let manager = await users.findOne({_id: ObjectID(owner.manager.id)})
                    if(req.body.status == 'Отправлено на рассмотрение'){
                        let email = {
                            type: 0,
                            employee: ` ${owner.surname} ${owner.name}`,
                            msg: 1,
                            to: manager.login
                        };
                        let sentMail = await sendMail(mail(email));
                        console.log(sentMail);
                    } else if(req.body.status == 'Отправлено на доработку'){
                        let email = {
                            type: 0,
                            employee: ` ${manager.surname} ${manager.name}`,
                            msg: 2,
                            to: owner.login
                        };
                        let sentMail = await sendMail(mail(email));
                        console.log(sentMail);
                    } else if(req.body.status == 'Подтверждено') {
                        let email = {
                            type: 0,
                            employee: ` ${manager.surname} ${manager.name}`,
                            msg: 4,
                            to: owner.login
                        };
                        let sentMail = await sendMail(mail(email));
                        console.log(sentMail);
                    } else if(req.body.status == 'Отправлен отчет') {
                        let email = {
                            type: 0,
                            employee: ` ${owner.surname} ${owner.name}`,
                            msg: 5,
                            to: manager.login
                        };
                        let sentMail = await sendMail(mail(email));
                        console.log(sentMail);
                    } else if(req.body.status == 'Отправлен отчет на доработку') {
                        let email = {
                            type: 0,
                            employee: ` ${manager.surname} ${manager.name}`,
                            msg: 6,
                            to: owner.login
                        };
                        let sentMail = await sendMail(mail(email));
                        console.log(sentMail);
                    } else if(req.body.status == 'Отчет утвержден') {
                        let email = {
                            type: 0,
                            employee: ` ${owner.surname} ${owner.name}`,
                            msg: 8,
                            to: manager.login
                        };
                        let sentMail = await sendMail(mail(email));
                        console.log(sentMail);
                        let month = new Date().getMonth();
                        if(month == 5 || month == 11){
                            let end_email = {
                                type: 0,
                                employee: ` ${owner.surname} ${owner.name}`,
                                msg: 9,
                                to: owner.login
                            };
                            let sentMail2 = await sendMail(mail(end_email));
                            console.log(sentMail2);
                        }
                    }
                    
                    await logs.insertOne({
                        user_id: decoded.user._id,
                        name: decoded.user.name+ ' '+ decoded.user.surname,
                        action: 'изменил(a) таблицу сотдруника '+owner.name+' '+owner.surname,
                        date: new Date()
                    });
                }
                await users.updateOne(
                    {_id: ObjectID(kpi.manager_id)},
                    {
                    $set: {
                            kpi_to_review: notify
                        }
                    }
                ); 
                await users.updateOne(
                    {_id: ObjectID(kpi.user_id)},
                    {
                    $set: {
                        kpi_notify: notify
                        }
                    }
                );
                
                let id = req.body._id;
                req.body.last_edit_time      = new Date();
                req.body.last_edit_person    = decoded.user.name+' '+decoded.user.surname;
                req.body.last_edit_person_id = decoded.user._id;
                delete req.body._id; 
                delete req.body.user_id; 
                delete req.body.manager_id;

                let editedKpi = await kpis.updateOne(
                    {'_id': ObjectID(id)},
                    {
                    $set: { ...req.body }
                    }
                );
                
                res.send(editedKpi);      
            } else {
                res.status(403).send('Error (Permission)');
            }
        } catch(err){
            res.status(500).send(err);
            throw err;
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
});

router.get('/get/surveys', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let dbase      = await db.db(dbname);
            let surveys      = dbase.collection('surveys');
            let  result = await surveys.find({}).toArray();
            res.send(result);
        } catch(err){
            res.status(500).send('Error');
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
})

router.get('/get/total/:year/:period', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded     = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase        = await db.db(dbname);
            let users        = dbase.collection('users')
            let totals       = dbase.collection('totals')
            let companies    = dbase.collection('companies')
            let kpis         = dbase.collection('kpis')
            let date = new Date();
            date.setFullYear(parseInt(req.params.year))
            if(parseInt(req.params.period) === 2) {
                date.setMonth(1);
                date.setDate(1);
                date.setHours(0);
                date.setFullYear(parseInt(req.params.year) + 1)
            } else {
                date.setMonth(5);
                date.setDate(1);
                date.setHours(0);
            }
            let d = new Date(date).getTime();
            let itogi        = await users.find({creation_date: {$lte: d}}, {projection: {department :1,
                surname: 1, name: 1, rank:1, manager:1, }}).toArray()
            let all_totals = []
            let results = itogi.map(async (element, index)  => {
                let kpi   = await kpis.findOne(
                    {
                     user_id: ObjectID(element._id),
                     year: req.params.year,
                     period: req.params.period
                    })
                if(kpi){
                    if(kpi.total) element.kpi = kpi.total
                    else element.kpi = 0
                }
                else element.kpi = 0
                let mark  = await totals.findOne({user_id: element._id.toString(), year: parseInt(req.params.year),
                    period: parseInt(req.params.period)}, 
                {projection: {user_id:1,s_mark:1, k_mark:1}})
                let total = 0
                if(mark){
                    for(let i = 0; i<mark.s_mark.length;i++) total=total+mark.s_mark[i].total 
                    for(let i = 0; i<mark.k_mark.length;i++) total=total+mark.k_mark[i].total 
                }
                element.mark = total
                all_totals.push(element)
            });
            Promise.all(results).then( async () => {
                var c_options = await companies.find().toArray()
                c_options.map(item => {
                    item.people = []
                })
                // var c_options = [
                //     {people: [], branches:[], id: '44c14bf18ab',   text:'HUMO Partners'},
                //     {people: [], branches:[], id: 'aysbfkajkjn',   text:'Финансовый департамент'},
                //     {people: [], branches:[], id: 'asdgasgeg44',   text:'HR-департамент'},
                //     {people: [], branches:[], id:'c7d90db000',   text:'УКТД_Отдел поставки оборудования'},
                //     {people: [], branches:[], id:'c7afisudbiusd',  text:'УКТД_Отдел строительной техники и оборудования'},
                //     {people: [], branches:[], id:'c7932n493n8938', text:'УКТД_Отдел маркетинга и продвижения'},
                //     {people: [], branches:[], id:'c7akkkkfkfkjaf', text:'УКТД_Китайский офис'},
                //     {people: [], branches:[], id:'df43f03e4',     text:'CLG'},
                //     {people: [], branches:[], id:'1asdfe96sdd',    text:'International Business Travel'},
                //     {people: [], branches:[], id:'37ub398hoiwe',  text:'GroundZero'},
                //     {people: [], branches:[], id:'sdsafasfasf',   text:'ИБЦ ТехноПлаза'},
                //     {people: [], branches:[], id:'837eiujiue88',  text:'MediTech'}, 
                // ]
                
                for(let i=0;i<all_totals.length;i++){
                    for(t=0;t<c_options.length;t++){
                        if(c_options[t].id == all_totals[i].department.id){
                            c_options[t].people.push(all_totals[i])
                        }                        
                    }
                }
                if(decoded.user.is_admin == 1){
                    res.send(c_options)
                }
                else if(decoded.user.is_admin == 2){
                    let arr = []
                    c_options.map((item) => {
                        if(decoded.user.department.id == item.id){
                            if(decoded.user.department.branch){
                                let department  = decoded.user.department.branch;
                                item.people.map(person => {
                                    if(person.department.branch){
                                        if((person._id == decoded.user._id || person.rank < decoded.user.rank) && department == person.department.branch){
                                            arr.push(person)
                                        }
                                    }
                                })                                
                            } else {
                                item.people.map(person => {
                                    if((person._id == decoded.user._id) || person.rank < decoded.user.rank){
                                        arr.push(person)
                                    }
                                })
                            }
                        }
                    })
                    res.send([{people: arr, id: decoded.user.department.id, text: decoded.user.department.name}]);
                }
                else {
                    try {
                        let arr = []
                        c_options.map((item) => {
                            if(decoded.user.department.id == item.id){
                                item.people.map(person => {
                                    if((person._id == decoded.user._id) || (person.manager && person.manager.id == decoded.user._id)){
                                        arr.push(person) 
                                    }
                                })
                            }
                        })
                        res.send([{people: arr, id: decoded.user.department.id, text: decoded.user.department.name}])
                    } catch (err) {
                        console.log(err);
                    }
                }
            })
        } catch(err){
            console.log(err)
            res.status(500).send('Error');
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
})

router.get('/get/logs/:last_id', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded     = await jwt.decode(req.headers['authorization'], secret.secret);
            if(decoded.user.is_admin == 1){
                let dbase     = await db.db(dbname);
                let logs      = dbase.collection('logs');
                var result = []
                console.log(null)
                if(req.params.last_id == 'null') {
                    result   = await logs.find({}).sort({_id: -1}).limit(20).toArray();
                } else { 
                    result = await logs.find({'_id': {'$lt': ObjectID(req.params.last_id)}}).sort({_id: -1}).limit(10).toArray();
                }
                console.log(result)
                res.send(result)
            } else {
                 res.status(403).send('Error (Permission)')
            }
        } catch(err){
            res.status(500).send(err);
            throw err;
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
});

router.get('/get/employees', async(req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded        = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase          = await db.db(dbname);
            let users          = dbase.collection('users');
            let kpis           = dbase.collection('kpis');
            var result = [];
            if(decoded.user.is_admin == 2){
                if(decoded.user.department.branch){
                    result = await users.find({
                        'department.id': decoded.user.department.id,
                        'department.branch': decoded.user.department.branch,
                        rank: { $lt: decoded.user.rank }
                    }).toArray();                    
                } else {
                    result = await users.find({
                        $or: [
                            {
                                'department.id': decoded.user.department.id,
                                rank: { $lt: decoded.user.rank }
                            },
                            {
                                'manager.id': decoded.user._id
                            }
                        ]
                    }).toArray();
                }
            } else if(decoded.user.is_admin == 1){
                  result   = await users.find({}).toArray();
            } else {
                 result    = await users.find({
                    department: decoded.user.department,
                    rank: { $lte: decoded.user.rank }}).toArray();
            }
            // if(decoded.user.is_admin !== 1){
                // let gavhar = await users.findOne({'department.id': 'asdgasgeg44', rank: 8});
                // gavhar.hr = true;
                // if(decoded.user.department.id != 'asdgasgeg44') result.push(gavhar)
            // }
            res.send(result);    

        } catch(err){
            console.log(err);
            res.status(500).send('Error');
        }
    } else {
        res.status(403).send('Permission error');
    }
});

router.get('/get/employee/:id/:year/:period', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded    = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase      = await db.db(dbname);
            let users      = dbase.collection('users');
            let kpis       = dbase.collection('kpis');
            let user       = await users.findOne({'_id': ObjectID(req.params.id.toString())});
            let employees  = await users.find({}).toArray();
            let kpi        = await kpis.findOne({'user_id': user._id,'year':req.params.year,'period':req.params.period});
            if(kpi == null){
                if(!user.manager) user.manager = {id:null}
                await kpis.insertOne({
                    user_id: user._id,
                    last_time: Date.now(),
                    manager_id: user.manager.id,
                    ...kpi_form,
                    period: req.params.period,
                    year: req.params.year,
                })
                kpi        = await kpis.findOne({'user_id': ObjectID(user._id.toString()),'year':req.params.year,'period':req.params.period});
            }
            // if(kpi) {
            //     if(!kpi.comments) kpi.comments = []
            //     if(!kpi.report_comments) kpi.report_comments = []
            //     if(!kpi.fact_approvements) kpi.fact_approvements = []
            // }
            // if(kpi == null){
            //     kpi        = await kpis.findOne({'user_id': user._id.toString});
            // }
            let eq = 0
           await employees.map((item) => {
                if(item.manager){
                    if(item.manager.id == user._id) {
                        eq=eq+1
                    }
                }
            })
            user.eq = eq;
            if(!user.manager){
                user.manager = {
                    id:null
                }
            } 
            if(!decoded.user.manager) decoded.user.manager = {id:null}
            if(decoded.user.is_admin == 1 || decoded.user._id == user.manager.id 
                || decoded.user._id == kpi.user_id || decoded.user.rank > user.rank){
                res.send({user: user, kpi: kpi});
            } else if((decoded.user.manager.id == user._id) || user.department.id == 'asdgasgeg44'){ 
                res.send({user: user, kpi: null})
            } else{
                res.status(403).send('Error (Permission)');
            }
        } catch(err){
            res.status(500).send('Error');
            throw err;
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
});

router.post('/delete/survey/:id', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded       = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase         = await db.db(dbname);
            let surveys       = dbase.collection('surveys');
            if(decoded.user.is_admin == 1){
                let removed = await surveys.remove({_id: ObjectID(req.params.id)});
                res.send('Deleted');
            } else {
                res.status(403).send('Error (Permission)');
            }
        } catch(err){
            res.status(500).send('Internal Error');
            throw err;
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
})

router.post('/edit/survey/:id', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded       = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase         = await db.db(dbname);
            let surveys       = dbase.collection('surveys');
            delete req.body._id;
            if(decoded.user.is_admin == 1){
                let edited = await surveys.updateOne({_id: ObjectID(req.params.id)}, { $set: {
                    ...req.body
                }});
                res.send(edited);
            } else {
                res.status(403).send('Error (Permission)');
            }
        } catch(err){
            res.status(500).send('Internal Error');
            throw err;
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
})

router.get('/get/surveys/:id', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let decoded       = await jwt.decode(req.headers['authorization'], secret.secret);
            let dbase         = await db.db(dbname);
            let surveys       = dbase.collection('surveys');
            let users         = dbase.collection('user');
            let marks         = dbase.collection('marks');
            let foundMarks    = await marks.findOne({marking_person: decoded.user._id, user_id: req.params.id});
            if(!foundMarks){
                let user = await users.findOne({_id: ObjectID(req.params.id)});
                let all_surveys = await surveys.find({ name: { $all: user.surveys } }).toArray();
                res.send(all_surveys);
            } else {
                res.send(foundMarks);
            }
        } catch(err){
            res.status(500).send('Internal Error');
            throw err;
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
});


router.post('/post/permission', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let dbase         = await db.db(dbname);
            let permissions   = dbase.collection('permissions');
            
            let setPermission = await permissions.updateOne(
                {name: 'kpi_permissions'},
                {
                    $set:{
                        name: 'kpi_permissions',
                        permissions: req.body.data,
                        period: req.body.period
                    },
                },{upsert: true});

                res.send({result:setPermission, ok:'Permission set'})
        } catch(err){
            res.status(500).send('Internal Error');
            throw err;
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
})

router.get('/get/permissions', async (req, res) => {
        try{
            let dbase      = await db.db(dbname);
            let surveys      = dbase.collection('permissions');
            let  result = await surveys.find({}).toArray();
            res.send(result);
        } catch(err){
            res.status(500).send('Error');
        }
});


router.get('/get/companies', async (req, res) => {
    try{
        let dbase   = await db.db(dbname);
        let companies = dbase.collection('companies');
        let  result = await companies.find({}).toArray();
        res.send(result);
    } catch(err){
        res.status(500).send('Error');
    }
});

router.post('/post/company', async (req, res) => {
    if(req.headers['authorization']){
        try{
            let dbase         = await db.db(dbname);
            let companies   = dbase.collection('companies');
            let newCompany = await companies.insertOne({
                id: req.body.id,
                name: req.body.name,
                branches: req.body.branches                
            })
                res.send(newCompany)
        } catch(err){
            res.status(500).send('Internal Error');
            throw err;
        }
    } else {
        res.status(403).send('Error (Permission)');
    }
});


router.post('/delete/company/:id', async (req, res) => {
    if(req.headers['authorization']){
        try {
            let dbase      = await db.db(dbname);
            let company    = dbase.collection('companies');
            let deleted    = await company.deleteOne({_id: ObjectID(req.params.id)})
            res.send(deleted);
        } catch (err){
            res.status(500).send('Internal Error')
            throw err;
        }
    } else { 
        res.status(403).send('Permission denied');
    }    
});


router.post('/edit/company/:id', async (req, res) => {
    if(req.headers['authorization']){
        try {
            let dbase     = await db.db(dbname);
            let company   = dbase.collection('companies');
            console.log(req.body)
            delete req.body._id;
            let edited    = await company.updateOne({_id: ObjectID(req.params.id)}, {$set: {
                ...req.body
            }})
            res.send(edited);
        } catch (err){
            res.status(500).send('Internal Error')
            throw err;
        }
    } else { 
        res.status(403).send('Permission denied');
    }    
});
module.exports = router;