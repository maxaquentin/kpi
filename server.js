const express = require('express');
const router  =  require('./server/api');
var   app     = express();
var   history = require('connect-history-api-fallback');

app.use(router);
app.use(history());
app.use('/ava-images', express.static(__dirname + '/server/images'))
app.use('/', express.static(__dirname + '/dist'));
console.log(__dirname)
let port = process.env.PORT || 3000
console.log(port);
app.listen(port, () => {
    console.log('Started on port ' + port);
})