import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: Home,
      children: [
        {
          path: '/',
          component: () => import('./components/GoalsTable.vue')
        },
        {
          path: '/employees-kpi',
          component: () => import('./views/EmployeeKpi.vue')
        },
        {
          path: '/competencies',
          component: () => import('./views/Competencies.vue')
        },
        {
          path: '/surveys',
          component: () => import('./views/Questions.vue')
        },
        {
          path: '/permissions',
          component: () => import('./views/Permissions.vue')
        },
        {
          path: '/total',
          component: () => import('./views/Totals.vue')
        },
        {
          path: '/logs',
          component: () => import('./views/Logs.vue')
        },
        {
          path: 'marks',
          component: () => import('./views/Marks.vue')
        },
        {
          path:'new-employee',
          component: () => import('./views/NewEmployee.vue'),
        },
        {
          path:'companies',
          component: () => import('./views/Companies.vue'),
        },
        {
          path:'feedback',
          component: () => import('./views/Feedback.vue'),
        }
      ],
      meta: { 
        requiresAuth: true,
        // is_admin : true
      }
    },
    { path: '/employee/:id',
     component: () => import('./views/Employee.vue'),
      children: [
        {
          path:'marking',
          name: 'marking',
          component: () => import('./views/Marking.vue'),
        },
        {
          path:'kpi',
          name: 'kpi',
          component: () => import('./components/GoalsTable.vue'),
        },
       
      ],
      meta: { 
        requiresAuth: true,
      }
    },
    {
      path: '/login',
      name: 'login',
       component: () => import('./views/Login.vue'),
    },
  ]
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.getItem('kpi-jwt-auth') == null ){
          next({
              path: '/login',
              params: { nextUrl: to.fullPath }
          })
      } else {
          let user = JSON.parse(localStorage.getItem('kpi-jwt-user'))
          if(to.matched.some(record => record.meta.is_admin)) {
              
          }else {
              next()
          }
      }
  } else if(to.matched.some(record => record.meta.guest)) {
      if(localStorage.getItem('jwt') == null){
          next()
      }
      else{
          next({ name: 'sms-receive'})
      }
  }else {
      next() 
  }
})

export default router