import Vue       from 'vue'
import App       from './App.vue'
import router    from './router'
import Loader    from '@/components/Loader.vue'
import axios               from 'axios'
import VueAxios            from 'vue-axios'
import ShardsVue           from 'shards-vue'
import VueTextareaAutosize from 'vue-textarea-autosize'
import i18n from './locales';

Vue.use(VueTextareaAutosize)
import 'bootstrap/dist/css/bootstrap.css'
import 'shards-ui/dist/css/shards.css'
Vue.use(ShardsVue);
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.component('Loader', Loader);

Vue.prototype.$url = 'https://kpi.hmpartners.uz'
Vue.prototype.$imageurl = 'https://kpi.hmpartners.uz/ava-images/'
new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
