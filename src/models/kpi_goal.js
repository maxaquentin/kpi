export default () => ({
    name: '', 
    kpi:'',
    weight: 0,
    units: '',
    plan_value: 0,
    fact_value: 0,
    goal_achieved: 0,
    goal_mark: 0,
    months: [
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
        {
            plan: 0,
            fact: 0,
            achieved:0
        },
    ],
});
